#!/usr/bin/env bash

foo() {

    # This variable has the 'local' modifier, so it will only be visible in the scope of the function foo().
    # Even after executing the foo() function the variable will not be available to other functions since it is out of scope.

    local STR1="this is a local string"
    echo "This echo is inside the foo() function, printing STR1: \"${STR1}\""

    # This is very useful to create local and closed-scope variables to handle, for example, more sensitive data.

}

bar() {

    # This variable has no modifier defined so it will default to a global variable.
    # This means that after calling the bar() function the variable will be set and available inside any scope.

    STR2="this is a global string"

}

main() {

    foo
    bar

    echo "This echo is inside the main() function, printing STR1: \"${STR1}\""
    echo "This echo is inside the main() function, printing STR2: \"${STR2}\""

}

main

echo "This echo is out of any function, printing STR2: \"${STR2}\""
