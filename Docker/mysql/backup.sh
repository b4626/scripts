#!/bin/bash

DOCKER=$(command -v docker)
GZIP=$(command -v gzip)

BACKUP_FOLDER="/path/to/backup/directory"
NOW="$(date '+%d%m%Y-%H%M')"

BACKUP_FILE="${BACKUP_FOLDER}/backup-${NOW}.sql"
MAX_BACKUPS=6

BACKUP_USER='foo'
BACKUP_PASSWORD='bar'
MYSQL_CONTAINER='container-name'

${DOCKER} exec -i ${MYSQL_CONTAINER} \
    mysqldump \
    --user=${BACKUP_USER} \
    --password=${BACKUP_PASSWORD} \
    --all-databases \
    --master-data \
    --single-transaction >${BACKUP_FILE}

${GZIP} -9 ${BACKUP_FILE}

ls -1t ${BACKUP_FOLDER}/backup-*.sql.gz | tail -n +$(($MAX_BACKUPS + 1)) | xargs rm -f
