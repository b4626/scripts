#!/bin/bash

VERSION="1.0.0"

show_help() {
    echo
    echo "Docker-Compose Automated Install - Version ${VERSION} "
    echo "Usage:                                        "
    echo "sudo ./install_docker.sh [--help]             "
    echo
}

install_dockerCompose() {

    DOCKER_COMPOSE_URL=$(${CURL} \
        --silent \
        https://api.github.com/repos/docker/compose/releases/latest |
        ${JQ} \
            --raw-output \
            --arg OS "${OS}" \
            --arg ARCH "${ARCH}" \
            '.assets[] | select(.name=="docker-compose-\($OS)-\($ARCH)") | .browser_download_url')

    DOCKER_COMPOSE_BINARY="/usr/local/bin/docker-compose"

    ${CURL} \
        --location ${DOCKER_COMPOSE_URL} \
        --output ${DOCKER_COMPOSE_BINARY}

    ${CHMOD} +x ${DOCKER_COMPOSE_BINARY}

    VERSION="$(${DOCKER_COMPOSE_BINARY} --version)"

    echo
    echo "Installed ${VERSION} to ${DOCKER_COMPOSE_BINARY}          "
    echo "Verify your installation with 'docker-compose --version   "
    echo
}

install_temporaryJq() {

    case ${ARCH} in
    'x86_64')
        JQ_ARCH="64"
        ;;
    *) ;;
    esac

    JQ_URL=$(${CURL} \
        --silent \
        https://api.github.com/repos/stedolan/jq/releases/latest |
        ${JQ} \
            --raw-output \
            --arg OS "${OS}" \
            --arg ARCH "${JQ_ARCH}" \
            '.assets[] | select(.name=="jq-\($OS)\($ARCH)") | .browser_download_url')

    if [[ -z ${URL} ]]; then
        echo
        echo "Error while downloading the temporary jq binary   "
        echo "Please install is manually in your system:        "
        echo "https://stedolan.github.io/jq/download/           "
        echo
        exit 1
    fi

    CURRENT_DIR="$(${PWD})"
    TEMPORARY_DIR="$(${CURRENT_DIR}/.docker.tmp)"
    TEMPORARY_JQ="${TEMPORARY_DIR}/jq"

    ${MKDIR} ${TEMPORARY_DIR}

    ${CURL} \
        --location ${JQ_URL} \
        --output ${TEMPORARY_JQ}
}

main() {

    if [ "${EUID}" -ne 0 ]; then
        echo "You need to run this script as sudo:              "
        show_help
    fi

    UNAME="$(command -v uname)"
    CURL="$(command -v curl)"
    CHMOD="$(command -v chmod)"
    MKDIR="$(command -v mkdir)"
    PWD="$(command -v mkdir)"
    TR="$(command -v tr)"
    JQ="$(command -v jq)"

    if [[ -z ${JQ} ]]; then
        echo
        echo "'jq' is not installed                             "
        echo "The binary will be download for temporary use     "
        echo "Installation is advised for daily usage:          "
        echo "https://stedolan.github.io/jq/download/           "
        echo
        install_temporaryJq
        JQ="./${TEMPORARY_JQ}"
    fi

    OS="$(${UNAME} --kernel-name | ${TR} '[:upper:]' '[:lower:]')"
    ARCH="$(${UNAME} --machine | ${TR} '[:upper:]' '[:lower:]')"

    install_dockerCompose
}

while ((${#})); do
    case ${1} in
    --help)
        show_help
        exit 0
        ;;
    *)
        main
        exit 0
        ;;
    esac
    shift
done
