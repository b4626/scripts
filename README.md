# Useful BASH scripts

This repository contains a set of useful [BASH](https://www.gnu.org/software/bash/) scripts that I developed and use daily in my DevOps/SysOps role.

All of these scripts are free of charge, feel free to fork the project and adapt them to your needs. If you feel like it, you may also contribute with improvements and corrections.

**Note:** some of these scripts are not *production* ready in a sense that they do not include redundancy and/or verifications during their execution. Use them with caution!